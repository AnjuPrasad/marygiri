# Generated by Django 3.2.9 on 2021-12-28 06:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='student',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fname', models.CharField(max_length=30, null=True)),
                ('lname', models.CharField(max_length=30, null=True)),
                ('dob', models.DateField(null=True)),
                ('email', models.EmailField(max_length=30, null=True)),
                ('gen', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female')], max_length=30, null=True)),
                ('subject', models.CharField(max_length=40, null=True)),
                ('mob', models.IntegerField(null=True)),
            ],
        ),
    ]
