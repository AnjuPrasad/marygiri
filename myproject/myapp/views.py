from django.http.response import HttpResponse
from django.shortcuts import render
from .models import student

def addstudent(request):
    if request.method=='POST':
        f=request.POST.get('first_name')
        l=request.POST.get('last_name')
        d=request.POST.get('birthday')
        g=request.POST.get('gender')
        e=request.POST.get('email')
        ph=request.POST.get('phone')
        sub=request.POST.get('subject')
        student.objects.create(fname=f,lname=l,dob=d,gen=g,email=e,subject=sub,mob=ph)
        return HttpResponse("Success!!")
    return render(request,"index.html")


