from django.contrib import admin
from django.contrib.admin.sites import site

from .models import student

admin.site.register(student)