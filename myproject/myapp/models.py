from django.db import models


class student(models.Model):
    fname=models.CharField(max_length=30,null=True)
    lname=models.CharField(max_length=30,null=True)
    dob=models.DateField(null=True)
    email=models.EmailField(max_length=30,null=True)
    gen=models.CharField(choices=(('Male','Male'),('Female','Female')),null=True,max_length=30)
    subject=models.CharField(max_length=40,null=True)
    mob=models.IntegerField(null=True)
